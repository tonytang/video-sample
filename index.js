var videoPlayerBuilders = burb.videoPlayer.getBuilders();
var configurationObject = videoPlayerBuilders.configuration;

configurationObject
    .withContainerElement($("#player-holder"))
    .withAccountId("719509184001") // Brightcove account number
    .withPlayerId("rJWSduqLl") // Brightcove Player ID
    .withAspectConfig("fullSize")
    .withVideoId("5410264841001") // Brightcove Video ID
    .withLegacySmartPlayerVideoId("5491036737001") // Brightcove Video ID
    .withPosterFrameImage("//assets.burberry.com/is/image/Burberryltd/8a64f0f4c5073c1dc558bd6f404b5868d8ed05ee?wid=1200")
    .withTheme("black")
    .withPreload("none");

var testPlayer = new burb.videoPlayer(configurationObject.build());

testPlayer.initialize();
